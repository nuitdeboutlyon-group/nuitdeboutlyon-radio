from node:4-onbuild
EXPOSE 8000 8001 8002
ADD ./config/config.json /config/config.json
CMD /usr/src/app/streammachine-cmd --config /config/config.json
